import React, { useState } from 'react';
import axios from 'axios';

import Calendar from 'react-calendar'
import 'react-calendar/dist/Calendar.css';

import moment from 'moment'
import momentTimeZones from 'moment-timezone'

import { getUser } from './Utils/Common';
import './css/custom.css';

import { API_BASE_URL } from './Constants';


let avlTimeSlots = [];
let showTimeSlots = {display: "none"};
let timeZone = moment.tz.guess();
let selectedDate = moment().format('dddd, MMMM DD');

function ScheduleEvent() {
  // setting of constants
  const [date, setDate] = useState(new Date());
  const [error, setError] = useState(null);
  const [state, setState] = useState(avlTimeSlots);

  // get loggedin user data
  const userData = getUser();

  // list of timezones
  const timeZoneList = momentTimeZones.tz.names();

  // change date function to get all available slots by calling API
  const changeDate = (newDate) => {
    let orgNewDate = newDate;
    newDate = moment(newDate).format('DD/MM/YYYY');
    setDate(newDate);

    selectedDate = moment(orgNewDate).format('dddd, MMMM DD');
    setDate(selectedDate);

    axios.get(API_BASE_URL + '/user/avlTimeSlots&userid=1&date=' + newDate).then(response => {
      avlTimeSlots = response.data.avlSlots;
      setState(avlTimeSlots);

      showTimeSlots = {};
      setState(showTimeSlots);
    }).catch(error => {
      if (error.response.status === 401) setError(error.response.data.message);
      else setError("Something went wrong. Please try again later.");
    });
  }

  // change timezone & set available time slots as per selected timezone
  const changeTimeZone = (newTimeZone) => {
    timeZone = newTimeZone;
    setDate(newTimeZone);
  }

  // rendering HTML
  return (
    <div className="calndrPg">
      <div className="left">
        <p>{userData ? userData.name : "User"}</p>
        <h3>60 Minute Meeting</h3>
      </div>

      <div className="right">
        <h3>Select Date & Time</h3>
        <div className="clndr">
          <Calendar
            onChange={date => changeDate(date)}
            minDate={moment().toDate()}
            tileDisabled={({ date }) => (date.getDay() === 0 || date.getDay() === 6)}
          />

          <div className="timezone">
            <select defaultValue={timeZone} onChange={e => changeTimeZone(e.target.value)}>
              {timeZoneList.map((timeZone, index) =>
                <option key={index} value={timeZone}>{timeZone}</option>
              )}
            </select>
          </div>
        </div>

        <div className="tmslot" style={showTimeSlots}>
          <p>{selectedDate}</p>
          <ul>
          {avlTimeSlots.map((timeSlot, index) =>
            <li key={index}>{moment.tz(new Date(moment().format('dddd, MMMM DD') + " " + timeSlot), timeZone).format("LT")}</li>
          )}
          </ul>
        </div>

        <div className="clear"></div>
      </div>

      <div className="clear"></div>
    </div>
  );
}

export default ScheduleEvent;
